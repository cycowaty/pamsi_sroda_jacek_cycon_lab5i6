//#include "generator.h"
#include "tablica.h"
#include "algorytmy.h"
#include "odwrot.h"
#include <iostream>
#include <fstream>
#include <time.h>

using namespace std;
void wygeneruj(int rozmiar, int ilosc, int zakres)
{
	algorytmy algorytm(rozmiar);
	fstream dane;
	dane.open("dane.txt", fstream::out, fstream::trunc);
	if (!dane.good())
    {
        cout << "Nie mog� otworzy� pliku";
        exit(1);
    }
    else
    {
		dane << "rozmiar : " << rozmiar << endl;
		dane << "ilosc : " << ilosc << endl;
		dane << "posortowanych : 0" << endl;
		srand (time(NULL));
		for(int j=0; j<ilosc ; j++)
		{
			for(int i=0; i<rozmiar ; ++i)
				dane << rand()%zakres << " ";

			dane << endl << endl;
		}

		dane << "rozmiar : " << rozmiar << endl;
		dane << "ilosc : " << ilosc << endl;
		dane << "posortowanych [%] : 25" << endl;
		srand (time(NULL));
		for(int j=0; j<ilosc ; j++)
		{
			int i=0;
			for(; i<rozmiar*0.25 ; ++i)
				dane << i << " ";
			for(; i<rozmiar; ++i)
				dane << rand()%zakres + rozmiar*0.25<< " ";

			dane << endl << endl;
		}

		dane << "rozmiar : " << rozmiar << endl;
		dane << "ilosc : " << ilosc << endl;
		dane << "posortowanych [%] : 50" << endl;
		srand (time(NULL));
		for(int j=0; j<ilosc ; j++)
		{
			int i=0;
			for(; i<rozmiar*0.5 ; ++i)
				dane << i << " ";
			for(; i<rozmiar; ++i)
				dane << rand()%zakres + rozmiar*0.5<< " ";

			dane << endl << endl;
		}

		dane << "rozmiar : " << rozmiar << endl;
		dane << "ilosc : " << ilosc << endl;
		dane << "posortowanych [%] : 75" << endl;
		srand (time(NULL));
		for(int j=0; j<ilosc ; j++)
		{
			int i=0;
			for(; i<rozmiar*0.75 ; ++i)
				dane << i << " ";
			for(; i<rozmiar; ++i)
				dane << rand()%zakres + rozmiar*0.75<< " ";

			dane << endl << endl;
		}

		dane << "rozmiar : " << rozmiar << endl;
		dane << "ilosc : " << ilosc << endl;
		dane << "posortowanych [%] : 95" << endl;
		srand (time(NULL));
		for(int j=0; j<ilosc ; j++)
		{
			int i=0;
			for(; i<rozmiar*0.95 ; ++i)
				dane << i << " ";
			for(; i<rozmiar; ++i)
				dane << rand()%zakres + rozmiar*0.95<< " ";

			dane << endl << endl;
		}

		dane << "rozmiar : " << rozmiar << endl;
		dane << "ilosc : " << ilosc << endl;
		dane << "posortowanych [%] : 99" << endl;
		srand (time(NULL));
		for(int j=0; j<ilosc ; j++)
		{
			int i=0;
			for(; i<rozmiar*0.99 ; ++i)
				dane << i << " ";
			for(; i<rozmiar; ++i)
				dane << rand()%zakres + rozmiar*0.99<< " ";

			dane << endl << endl;
		}

		dane << "rozmiar : " << rozmiar << endl;
		dane << "ilosc : " << ilosc << endl;
		dane << "posortowanych [%] : 99.7" << endl;
		srand (time(NULL));
		for(int j=0; j<ilosc ; j++)
		{
			int i=0;
			for(; i<rozmiar*0.997 ; ++i)
				dane << i << " ";
			for(; i<rozmiar; ++i)
				dane << rand()%zakres + rozmiar*0.997<< " ";

			dane << endl << endl;
		}

		dane << "rozmiar : " << rozmiar << endl;
		dane << "ilosc : " << ilosc << endl;
		dane << "posortowanych [%] : 100" << endl;

		for(int j=0; j<ilosc ; j++)
		{
			srand (time(NULL));
			tablica *tab = new tablica(rozmiar);
			tablica *tab2 = new tablica(rozmiar);
			tab->wypelnij(zakres);
			algorytm.introspektywne(tab->tab, rozmiar);
			odwroctablice(tab->tab, tab2->tab, rozmiar);
			for(int i=0; i<rozmiar ; ++i)
				dane << tab2->tab[i] << " ";
			dane << endl << endl;
			delete tab;
			delete tab2;
		}
	}
	dane.flush();
	dane.close();
}
