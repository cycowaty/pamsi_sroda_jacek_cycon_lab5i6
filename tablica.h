
#pragma once

class tablica
{
	// x - zakres wartosci w tablicy
	// n - rozmiar tablicy
	int n,x;

public:

	// *tab - wskaznik do tablicy
	int *tab;

	// Konstruktor alokujacy dynamicznie tablice o podanych parametrach
	tablica(int rozmiar);

	// Destruktor zwalniajacy pamiec z przypisana do niej tablica
	~tablica(void);

	// Metoda wpisujaca losowe liczby do tablicy mieszczace sie w podanym zakresie
	void wypelnij(int zakres);

	// Metoda wyswietlajaca zawartosc tablicy na standardowym wyjsciu
	void wyswietl();

	// Metoda wyszukujaca najmniejszy element w tablicy i wypisujaca go
	void findMin();

	// Metoda wyszukujaca najwiekszy element w tablicy i wypisujaca go
	void findMax();
};
