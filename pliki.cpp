#include "pliki.h"
#include "tester.cpp"
#include "algorytmy.h"
#include <windows.h>
#include <iostream>
#include <fstream>
#include "algorytmy.cpp"
#include "tablica.cpp"

using namespace std;

void przetworzdane()
{
	fstream dane, wyniki;
	wyniki.open("wyniki.txt", fstream::out);
	float ms =0, dzielnik;
	unsigned __int64 freq, counterStart, counterStop;

	int rozmiar,ilosc,ilosc2, test=0;
    float posortowanych;
	for(int alg = 1; alg < 4; ++alg)
	{
		cout << "Algorytm nr: " << alg << endl;
		dane.open("dane.txt");
		for(int x = 1; x < 9 ; ++x)
		{
			dane.ignore(256,':');
			dane >> rozmiar;
			dane.ignore(256,':');
			dane >> ilosc;
			dane.ignore(256,':');
			dane >> posortowanych;
			int nr=1;
			ilosc2=ilosc;
			algorytmy algorytm(rozmiar);
			dzielnik = static_cast<float> (ilosc);
			tablica *tab = new tablica(rozmiar);
			cout << "Krok " << x << " z 8" << endl;
			while(ilosc2 > 0)
			{
				for(int i = 0; i < rozmiar; ++i) dane>>tab->tab[i];
				dane.get();

				QueryPerformanceFrequency(reinterpret_cast<LARGE_INTEGER*> (&freq));
				QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*> (&counterStart));

				if(alg==1)
					algorytm.mergesort(tab->tab,0,rozmiar-1);
				if(alg==2)
					algorytm.quicksort(tab->tab, 0, rozmiar-1);
				if(alg==3)
					algorytm.introspektywne(tab->tab,rozmiar);

				QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*> (&counterStop));
				ms += (static_cast<float> (counterStop) - counterStart) / freq * 1000;
				test += tester(tab,rozmiar);
				--ilosc2;
				++nr;
			}
			if(test == 0)
			{
				wyniki << "Sredni czas wykonania algorytmu nr " << alg << " dla " << ilosc << " tablic o rozmiarze " << rozmiar;
				wyniki << " posortowanych w : " << posortowanych << "% wynosi: " << ms/ilosc << endl << endl;
				ms = 0;
			}
			else
				wyniki << " Tablice zosta�y �le posortowane. " << endl;
			delete tab;
		}
		wyniki << "--------------------------------------------------------------------------------------------------------------" << endl;
		dane.close();
	}
		wyniki.flush();
		wyniki.close();
	}



