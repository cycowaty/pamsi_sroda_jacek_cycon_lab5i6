
#include "tablica.h"
#include <time.h>
#include <iostream>
#include <cstdlib>
using namespace std;

tablica::tablica(int rozmiar)
{
	tab = new int[rozmiar];
	x = 0;
	n = rozmiar;
}

tablica::~tablica(void)
{
	delete tab;
}

void tablica::wypelnij(int zakres)
{
	x=zakres; // przeslanie do zmiennej zakresu
	srand (time(NULL));
	for(int i=0; i<n ; ++i)
		tab[i]=rand()%zakres;
}

void tablica::wyswietl()
{
	for(int i=0; i<n; ++i)
		cout << tab[i]<< " ";
		cout << endl;
}

void tablica::findMin()
{
	int y=x;  // zmienna przechowywujaca aktualnie najmniesza wartosc

	for(int i=0; i<n; ++i)
	{
		if(tab[i]< y)
			y = tab[i];
	}

	cout << "Najmniejsza wartosc w tablicy to: " << y << endl;
}

void tablica::findMax()
{
	int y=0; // zmienna przechowywujaca aktualnie najwieksza wartosc

	for(int i=0; i<n; ++i)
	{
		if(tab[i] > y)
			y = tab[i];
	}

	cout << "Najwieksza wartosc w tablicy to: " << y << endl;
}
