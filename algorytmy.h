
#pragma once
#include "tablica.h"
class algorytmy
{

	int *tpom;

public:
	algorytmy(int rozmiar);
	~algorytmy(void);

	/*
	Metoda wykonujaca sortowanie przez kopcowanie
	*/
	void kopcowanie(int *tab, int rozmiar);
	/*
	Metody wykonujace sortowanie introspektywne
	*/
	void introspektywne(int *tab, int N);

	void introsort(int *tab, int N, int M);

	int Partition (int *Tab, int L, int R);

	void MedianOfThree (int *Tab, int &L, int &R);

	void Exchange (int *Tab, int i, int j);

	void Insertion_Sort (int *tab, int N);

	/*
	Metoda wykonująca sortowanie szybkie
	*/
	void quicksort(int *tab, int lewy, int prawy);

	/*
	Metody wykonująca sortowanie przez scalanie
	*/
	void merge(int *tab, int pocz, int sr, int kon);

	void mergesort(int *tab, int pocz, int kon);
};

