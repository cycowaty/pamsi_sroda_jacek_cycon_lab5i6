
#include "algorytmy.h"
#include "math.h"
#include <iostream>
//#define M_LN2      0.693147180559945309417

using namespace std;

algorytmy::algorytmy(int rozmiar)
{
	tpom = new int[rozmiar+1];
}

algorytmy::~algorytmy(void)
{
	delete tpom;
}
void algorytmy::merge(int *tab, int pocz, int sr, int kon)
{
	int i,j,k;

	i = pocz;
	j = sr + 1;
	k = 0;

	while(i<=sr && j <=kon)  // sortowanie i przeniesienie danych z tablicy pomocniczej do glownej
	{
		if(tab[j] < tab[i])
		{
			tpom[k]=tab[j];
			++j;
		}
		else
		{
			tpom[k]=tab[i];
			++i;
		}
		++k;
	}

	if ( i <= sr)
	{
		while(i <= sr)
		{
			tpom[k] = tab[i];
			++i;
			++k;
		}
	}
	else
	{
		while(j <= kon)
		{
			tpom[k] = tab[j];
			++j;
			++k;
		}
	}
	for( i=pocz; i<=kon; ++i)
		tab[i] = tpom[i-pocz];
}

void algorytmy::mergesort(int *tab, int pocz, int kon)
{
	int sr;
	if(pocz<kon)
	{
		sr = (pocz+kon)/2;
		mergesort(tab, pocz, sr);
		mergesort(tab, sr+1, kon);
		merge(tab,pocz,sr, kon);
	}
}

void algorytmy::quicksort(int *tab, int lewy, int prawy)
{
	int i = lewy;
	int j = prawy;
	int x = tab[(lewy+prawy)/2];
	do
	{
		while(tab[i]<x) i++;
		while(tab[j]>x) j--;
        if(i<=j)
		{
			int tmp=tab[i];
            tab[i]=tab[j];
            tab[j]=tmp;
            i++;
            j--;
		}
	}
	while(i<=j);
     if(lewy<j) quicksort(tab,lewy,j);
     if(prawy>i) quicksort(tab,i,prawy);
}

void algorytmy::introspektywne(int *tab, int N)
{
  introsort(tab,N,floor(2*log((float)N)/M_LN2));
  Insertion_Sort(tab,N);
}

void algorytmy::introsort(int *tab, int N, int M)
{
	int i;
	if( M <= 0)
	{
		kopcowanie(tab, N);
		return;
	}
	i = Partition(tab, 0, N);
	if (i>9)
		introsort(tab, i, M-1);
	if (N-1-i>9)
		introsort(tab+i+1, N-1-i, M-1);
}

int algorytmy::Partition (int *Tab, int L, int R)
{
  int i, j;
  if (R>=3)
    MedianOfThree(Tab,L,R);
  for (i=L, j=R-2; ; )
  {
    for ( ; Tab[i]<Tab[R-1]; ++i);
    for ( ; j>=L && Tab[j]>Tab[R-1]; --j);
    if (i<j)
      Exchange(Tab,i++,j--);
    else break;
  }
  Exchange(Tab,i,R-1);
  return i;
}

void algorytmy::MedianOfThree (int *Tab, int &L, int &R)
{
  if (Tab[++L-1]>Tab[--R])
    Exchange(Tab,L-1,R);
  if (Tab[L-1]>Tab[R/2])
    Exchange(Tab,L-1,R/2);
  if (Tab[R/2]>Tab[R])
    Exchange(Tab,R/2,R);
  Exchange(Tab,R/2,R-1);
}
void algorytmy::Exchange (int *Tab, int i, int j)
{
  int temp;
  temp=Tab[i];
  Tab[i]=Tab[j];
  Tab[j]=temp;
}

void algorytmy::Insertion_Sort (int *tab, int N)
{
  int i, j;
  int temp;
  for (i=1; i<N; ++i)
  {
    temp=tab[i];
    for (j=i; j>0 && temp<tab[j-1]; --j)
      tab[j]=tab[j-1];
    tab[j]=temp;
  }
}

void algorytmy::kopcowanie(int *tab, int rozmiar)
{
	int tmp;

	for(int i = 0; i < rozmiar; ++i)
	{
		for(int j = i; j != 0; j=(j-1)/2)
		{
			if(tab[j]>tab[(j-1)/2])
			{
				tmp=tab[j];
				tab[j]=tab[(j-1)/2];
				tab[(j-1)/2]=tmp;
			}
		}
	}
}
