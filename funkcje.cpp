
#include <iostream>
#include <fstream>
#include <time.h>
#include "algorytmy.cpp"
#include "tablica.cpp"
#include "funkcje.h"
#include <windows.h>
#include <cstdlib>
#include <math.h>

using namespace std;
void wygeneruj(int rozmiar, int ilosc, int zakres)
{
	algorytmy algorytm(rozmiar);
	fstream dane;
	dane.open("dane.txt", fstream::out);
	if (!dane.good())
    {
        cout << "Nie mog� otworzy� pliku";
        exit(1);
    }
    else
    {
		dane << "rozmiar : " << rozmiar << endl;
		dane << "ilosc : " << ilosc << endl;
		dane << "posortowanych : 0" << endl;
		srand (time(NULL));
		for(int j=0; j<ilosc ; j++)
		{
			for(int i=0; i<rozmiar ; ++i)
				dane << rand()%zakres << " ";

			dane << endl << endl;
		}

		dane << "rozmiar : " << rozmiar << endl;
		dane << "ilosc : " << ilosc << endl;
		dane << "posortowanych [%] : 25" << endl;
		srand (time(NULL));
		for(int j=0; j<ilosc ; j++)
		{
			int i=0;
			for(; i<rozmiar*0.25 ; ++i)
				dane << i << " ";
			for(; i<rozmiar; ++i)
				dane << rand()%zakres + rozmiar*0.25<< " ";

			dane << endl << endl;
		}

		dane << "rozmiar : " << rozmiar << endl;
		dane << "ilosc : " << ilosc << endl;
		dane << "posortowanych [%] : 50" << endl;
		srand (time(NULL));
		for(int j=0; j<ilosc ; j++)
		{
			int i=0;
			for(; i<rozmiar*0.5 ; ++i)
				dane << i << " ";
			for(; i<rozmiar; ++i)
				dane << rand()%zakres + rozmiar*0.5<< " ";

			dane << endl << endl;
		}

		dane << "rozmiar : " << rozmiar << endl;
		dane << "ilosc : " << ilosc << endl;
		dane << "posortowanych [%] : 75" << endl;
		srand (time(NULL));
		for(int j=0; j<ilosc ; j++)
		{
			int i=0;
			for(; i<rozmiar*0.75 ; ++i)
				dane << i << " ";
			for(; i<rozmiar; ++i)
				dane << rand()%zakres + rozmiar*0.75<< " ";

			dane << endl << endl;
		}

		dane << "rozmiar : " << rozmiar << endl;
		dane << "ilosc : " << ilosc << endl;
		dane << "posortowanych [%] : 95" << endl;
		srand (time(NULL));
		for(int j=0; j<ilosc ; j++)
		{
			int i=0;
			for(; i<rozmiar*0.95 ; ++i)
				dane << i << " ";
			for(; i<rozmiar; ++i)
				dane << rand()%zakres + rozmiar*0.95<< " ";

			dane << endl << endl;
		}

		dane << "rozmiar : " << rozmiar << endl;
		dane << "ilosc : " << ilosc << endl;
		dane << "posortowanych [%] : 99" << endl;
		srand (time(NULL));
		for(int j=0; j<ilosc ; j++)
		{
			int i=0;
			for(; i<rozmiar*0.99 ; ++i)
				dane << i << " ";
			for(; i<rozmiar; ++i)
				dane << rand()%zakres + rozmiar*0.99<< " ";

			dane << endl << endl;
		}

		dane << "rozmiar : " << rozmiar << endl;
		dane << "ilosc : " << ilosc << endl;
		dane << "posortowanych [%] : 99.7" << endl;
		srand (time(NULL));
		for(int j=0; j<ilosc ; j++)
		{
			int i=0;
			for(; i<rozmiar*0.997 ; ++i)
				dane << i << " ";
			for(; i<rozmiar; ++i)
				dane << rand()%zakres + rozmiar*0.997<< " ";

			dane << endl << endl;
		}

		dane << "rozmiar : " << rozmiar << endl;
		dane << "ilosc : " << ilosc << endl;
		dane << "posortowanych [%] : 100" << endl;

		for(int j=0; j<ilosc ; j++)
		{
			srand (time(NULL));
			tablica *tab = new tablica(rozmiar);
			tablica *tab2 = new tablica(rozmiar);
			tab->wypelnij(zakres);
			algorytm.introspektywne(tab->tab, rozmiar);
			odwroctablice(tab->tab, tab2->tab, rozmiar);
			for(int i=0; i<rozmiar ; ++i)
				dane << tab2->tab[i] << " ";
			dane << endl << endl;
			delete tab;
			delete tab2;
		}
	}
	dane.flush();
	dane.close();
}

void odwroctablice(int *tab, int *tab2, int rozmiar)
{
	for(int i = 0; i < rozmiar; ++i)
		tab2[i]=tab[rozmiar - i -1];
}

int tester(tablica *tab, int rozmiar)
{
	for(int i = 0; i < rozmiar -1; ++i)
	{
		if(tab->tab[i]>tab->tab[i+1])
			return 1;
	}
	return 0;
}

void przetworzdane()
{
	fstream dane, wyniki;
	wyniki.open("wyniki50k.txt", fstream::out);
	float ms =0, dzielnik;
	unsigned __int64 freq, counterStart, counterStop;

	int rozmiar,ilosc,ilosc2, test=0;
    float posortowanych;
	for(int alg = 1; alg < 4; ++alg)
	{
		cout << "Algorytm nr: " << alg << endl;
		dane.open("dane.txt");
		for(int x = 1; x < 9 ; ++x)
		{
			dane.ignore(256,':');
			dane >> rozmiar;
			dane.ignore(256,':');
			dane >> ilosc;
			dane.ignore(256,':');
			dane >> posortowanych;
			int nr=1;
			ilosc2=ilosc;
			algorytmy algorytm(rozmiar);
			dzielnik = static_cast<float> (ilosc);
			tablica *tab = new tablica(rozmiar);
			cout << "Krok " << x << " z 8" << endl;
			while(ilosc2 > 0)
			{
				for(int i = 0; i < rozmiar; ++i) dane>>tab->tab[i];
				dane.get();

				QueryPerformanceFrequency(reinterpret_cast<LARGE_INTEGER*> (&freq));
				QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*> (&counterStart));

				if(alg==1)
					algorytm.mergesort(tab->tab,0,rozmiar-1);
				if(alg==2)
					algorytm.quicksort(tab->tab, 0, rozmiar-1);
				if(alg==3)
					algorytm.introspektywne(tab->tab,rozmiar);

				QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*> (&counterStop));
				ms += (static_cast<float> (counterStop) - counterStart) / freq * 1000;
				test += tester(tab,rozmiar);
				--ilosc2;
				++nr;
			}
			if(test == 0)
			{
				wyniki << "Sredni czas wykonania algorytmu nr " << alg << " dla " << ilosc << " tablic o rozmiarze " << rozmiar;
				wyniki << " posortowanych w : " << posortowanych << "% wynosi: " << ms/ilosc << endl << endl;
				ms = 0;
			}
			else
				wyniki << " Tablice zosta�y �le posortowane. " << endl;
			delete tab;
		}
		wyniki << "--------------------------------------------------------------------------------------------------------------" << endl;
		dane.close();
	}
		wyniki.flush();
		wyniki.close();
	}
